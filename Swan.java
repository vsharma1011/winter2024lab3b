public class Swan {
	public boolean isFlying;
	public String bodyColor;
	public int insectsEaten;
	
	public void flyingSpeed(){
		if(insectsEaten > 10){
			System.out.println("The swan is flying slowly");
		}
		else{
			System.out.println("The swan is speeding across the sky");
		}
	}
	
	public void makesNoise(){
		if(isFlying){
			System.out.println("QUACK!");
		}
		else{
			System.out.println("**silence**");
		}
	}
}