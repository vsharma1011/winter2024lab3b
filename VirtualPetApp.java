import java.util.Scanner;	

public class VirtualPetApp {
	public static void main(String[] arg){
		Scanner reader = new Scanner(System.in);
		Swan[] flock = new Swan[4];
		
		for(int i=0; i < flock.length; i++){
			flock[i] = new Swan();
			System.out.println("Is the swan " + i + " flying?");
			flock[i].isFlying = Boolean.parseBoolean(reader.nextLine());
			System.out.println("What color is swan " + i + "?");
			flock[i].bodyColor = reader.nextLine();
			System.out.println("How many insects has swan " + i + " eaten?");
			flock[i].insectsEaten = Integer.parseInt(reader.nextLine());
		}
		System.out.println("Swan 3 is flying = " + flock[3].isFlying);
		System.out.println("Swan 3 is " + flock[3].bodyColor);
		System.out.println("Swan 3 is has eaten " + flock[3].insectsEaten);
		
		flock[0].flyingSpeed();
		flock[0].makesNoise();
	}
}